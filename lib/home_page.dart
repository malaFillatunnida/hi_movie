import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:hi_movie/colors.dart';
import 'package:hi_movie/model/MovieModel.dart';
import 'package:hi_movie/route_app.dart';
import 'package:hi_movie/theme.dart';
import 'package:unicons/unicons.dart';

import 'movie_detail_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<String> listCategory = ['Asia', 'Keluarga', 'Anime', 'Horror', 'Mystery', 'Drama', 'Sci-Fi'];

  final List<MovieModel> listTrendAsset = [];

  final List<MovieModel> listTVSeriesAsset = [];

  final List<String> listCarousel = [
    'assets/landscape_1917.jpeg',
    'assets/landscape_captainamerica.jpeg',
    'assets/landscape_gravity.jpeg',
    'assets/landscape_impossible.jpeg',
  ];

  Widget header() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 16, 12, 8),
      child: Row(
        children: [
          SizedBox(
            height: 24,
            child: Image.asset('assets/ic_main.png'),
          ),
          const Spacer(),
          const SizedBox(
            height: 24,
            child: Icon(
              UniconsLine.airplay,
              size: 24,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 24,
            child: Icon(
              UniconsLine.user_circle,
              size: 24,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 24,
            child: Icon(
              UniconsLine.search,
              size: 24,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget carouselBody() {
    return SizedBox(
      width: double.infinity,
      height: 240,
      child: Swiper(
        itemCount: 4,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.only(right: 16, left: 16),
            child: SizedBox(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Image.asset(listCarousel[index]),
                ),
              ),
            ),
          );
        },
        autoplay: true,
        autoplayDelay: 2500,
        pagination: const SwiperPagination(alignment: Alignment.bottomCenter),
      ),
    );
  }

  Widget category() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      child: Container(
        margin: const EdgeInsets.only(
          top: 16,
          left: 16,
          right: 16,
        ),
        child: Row(
          children: listCategory
              .map((item) => Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12,
                      vertical: 8,
                    ),
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        border: Border.all(
                          color: colorGrey.withOpacity(0.5),
                        )),
                    child: Text(
                      item,
                      style: textTitle,
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }

  Widget title(String title) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Text(
        title,
        style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w900),
      ),
    );
  }

  Widget cardMovie(List<MovieModel> listTrendAsset) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: const BouncingScrollPhysics(),
      child: Container(
        margin: const EdgeInsets.only(left: 16, right: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: listTrendAsset.map((iniHasilLoopNya) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute<void>(builder: (BuildContext context) {
                  return MovieDetailPage(dataFilm: iniHasilLoopNya);
                }));
              },
              child: Container(
                margin: const EdgeInsets.only(right: 8),
                child: SizedBox(
                  width: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: const BorderRadius.all(Radius.circular(12)),
                        child: Container(
                          width: 250,
                          height: 250,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(iniHasilLoopNya.path ?? ''),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }

  @override
  void initState() {
    listTVSeriesAsset.add(MovieModel(
        id: 1,
        path: 'assets/asset_missionimpossible.jpeg',
        title: 'Mission: Impossible - Dead Reckoning Part One',
        year: '2023',
        duration: '2h 43m',
        description: 'Ethan Hunt and his IMF team must track down a dangerous weapon before it falls into the wrong hands.',
        director: 'Christopher McQuarrie'));

    listTVSeriesAsset.add(MovieModel(
      id: 2,
      path: 'assets/asset_reptile.jpeg',
      title: 'Reptile',
      year: '2023',
      duration: '2h 14m',
      category: 'Crime',
      description:
          'Tom Nichols is a hardened New England detective, unflinching in his pursuit of a case where nothing is as it seems and it begins to dismantle the illusions in his own life. Director',
      director: 'Grant Singer',
    ));

    listTVSeriesAsset.add(MovieModel(
      id: 3,
      path: 'assets/asset_sawx.jpeg',
      title: 'SawX',
      year: '2023',
      duration: '1h 58m',
      category: 'Horror',
      description:
          'A sick and desperate John travels to Mexico for a risky and experimental medical procedure in hopes of a miracle cure for his cancer only to discover the entire operation is a scam to defraud the most vulnerable.',
      director: 'Kevin Greutert',
    ));

    listTVSeriesAsset.add(MovieModel(
      id: 4,
      path: 'assets/asset_thecreator.jpeg',
      title: 'Five Nights at Freddy\'s',
      year: '2023',
      duration: '2h 13m',
      category: 'Adventure',
      description: 'Against the backdrop of a war between humans and robots with artificial intelligence, a former soldier finds the secret weapon, a robot in the form of a young child.',
      director: 'Gareth Edwards',
    ));

    listTVSeriesAsset.add(MovieModel(
      id: 5,
      path: 'assets/asset_totallykiller.jpeg',
      title: 'Totally Killer',
      year: '2023',
      duration: '1h 46m',
      category: 'Comedy',
      description:
          'When the infamous "Sweet Sixteen Killer" returns 35 years after his first murder spree to claim another victim, 17-year-old Jamie accidentally travels back in time to 1987, determined to stop the killer before he can start.',
      director: 'Nahnatchka Khan',
    ));

    listTrendAsset.add(MovieModel(
      id: 1,
      path: 'assets/asset_burial.jpeg',
      title: 'The Burrial',
      year: '2023',
      duration: '2h 6m',
      category: 'Drama',
      description: 'Inspired by true events, a lawyer helps a funeral home owner save his family business from a corporate behemoth, exposing a complex web of race, power, and injustice.',
      director: 'Maggie Betts',
    ));

    listTrendAsset.add(MovieModel(
      id: 2,
      path: 'assets/asset_expendables.jpeg',
      title: 'Expend4bles',
      year: '2023',
      duration: '1h 43m',
      category: 'Action',
      description: 'Armed with every weapon they can get their hands on, the Expendables are the world\'s last line of defense and the team that gets called when all other options are off the table.',
      director: 'Scott Waugh',
    ));

    listTrendAsset.add(MovieModel(
      id: 3,
      path: 'assets/asset_fairplay.jpg',
      title: 'Fair Play',
      year: '2023',
      duration: '1h 53m',
      category: 'Mystery',
      description: 'An unexpected promotion at a cutthroat hedge fund pushes a young couple\'s relationship to the brink, threatening to unravel far more than their recent engagement.',
      director: 'Chloe Domont',
    ));

    listTrendAsset.add(MovieModel(
      id: 4,
      path: 'assets/asset_fivenights.jpg',
      title: 'Five Nights at Freddy\'s',
      year: '2023',
      duration: '1h 49m',
      category: 'Thriller',
      description:
          'A troubled security guard begins working at Freddy Fazbear\'s Pizza. During his first night on the job, he realizes that the night shift won\'t be so easy to get through. Pretty soon he will unveil what actually happened at Freddy\'s.',
      director: 'Emma Tammi',
    ));

    listTrendAsset.add(MovieModel(
      id: 5,
      path: 'assets/asset_flowermoon.jpeg',
      title: 'Killers of the Flower Moon',
      year: '2023',
      duration: '3h 26m',
      category: 'History',
      description: 'When oil is discovered in 1920s Oklahoma under Osage Nation land, the Osage people are murdered one by one - until the FBI steps in to unravel the mystery.',
      director: 'Martin Scorsese',
    ));

    setState(() {
      listTVSeriesAsset;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorAccent,
        title: Text(
          'HI MOVIE',
          style: textTitle,
        ),
      ),
      backgroundColor: colorAccent,
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          header(),
          const SizedBox(
            height: 8,
          ),
          carouselBody(),
          category(),
          title('Sedang tren sekarang'),
          cardMovie(listTrendAsset),
          title('TV Series'),
          cardMovie(listTVSeriesAsset),
        ],
      ),
    );
  }
}
