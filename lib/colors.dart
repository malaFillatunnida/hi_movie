

import 'dart:ui';

Color get colorPrimary => const Color(0xff1AACAC); //0xff7b121d
Color get colorPrimaryDark => const Color(0xff1AACAC);
Color get colorBackground => const Color(0xff0b0d10);
Color get colorHint => const Color(0xff161a1f);
Color get colorCard => const Color(0xff111518);
Color get colorAccent => const Color(0xFF232D3F);
Color get colorGrey => const Color(0xff6C757D);