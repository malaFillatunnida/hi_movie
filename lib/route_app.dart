import 'package:flutter/material.dart';
import 'package:hi_movie/home_page.dart';
import 'package:hi_movie/model/MovieModel.dart';
import 'package:hi_movie/movie_detail_page.dart';
import 'package:hi_movie/splash_page.dart';

class NavigatorRoutes {
  static const String root = '/';
  static const String home = '/home';
}

Route routesApp(RouteSettings setting) {
  switch (setting.name){
    case NavigatorRoutes.root:
      return MaterialPageRoute(builder: (_) => const SplashPage());
    case NavigatorRoutes.home:
      return MaterialPageRoute(builder: (_) => const HomePage());
    default:
      throw 'Navigasi tidak ditemukan';
  }
}
