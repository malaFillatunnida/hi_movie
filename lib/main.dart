import 'package:flutter/material.dart';
import 'package:hi_movie/route_app.dart';
import 'package:hi_movie/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.dark,
      title: 'HiMovie',
      theme: hiThemeData(context),
      initialRoute: NavigatorRoutes.root,
      onGenerateRoute: routesApp,
    );
  }
}
