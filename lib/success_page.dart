import 'package:flutter/material.dart';
import 'package:hi_movie/theme.dart';
import 'package:unicons/unicons.dart';

import 'colors.dart';

class SuccesPage extends StatelessWidget {
  const SuccesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 240, left: 16, right: 16),
            child: Center(
              child: SizedBox(
                height: 100,
                width: 240,
                child: Icon(
                  UniconsLine.bill,
                  size: 100,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Text(
            'Pembayaran berhasil',
            style: textTitle.copyWith(fontSize: 24),
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            'Silahkan nikmati semua fitur yang ada di aplikasi kami, tanpa terkecuali',
            textAlign: TextAlign.center,
            style: textTitle.copyWith(fontSize: 16),
          ),
          const SizedBox(
            height: 8,
          ),
          Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 24,
                  width: 24,
                  child: Icon(
                    UniconsLine.calender,
                    size: 24,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  width: 4,
                ),
                Text(
                  '04 November 2023 11:09',
                  style: textTitle.copyWith(fontSize: 16),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            'TRX ID : 123456',
            style: textTitle.copyWith(fontSize: 16),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text(
              '< Kembali',
              style: textTitle.copyWith(fontSize: 16, color: Colors.white),
            ),
          ),
          const Spacer(),
          Center(
            child: SizedBox(
              width: 150,
              height: 150,
              child: Image.asset('assets/ic_main.png'),
            ),
          )
        ],
      ),
    );
  }
}
