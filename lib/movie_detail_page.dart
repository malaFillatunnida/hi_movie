import 'package:flutter/material.dart';
import 'package:hi_movie/colors.dart';
import 'package:hi_movie/payment_page.dart';
import 'package:hi_movie/theme.dart';
import 'package:unicons/unicons.dart';

import 'model/MovieModel.dart';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key, required this.dataFilm}) : super(key: key);

  final MovieModel dataFilm;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final List<String> listActor = ['assets/actor_1.jpg', 'assets/actor_2.jpg', 'assets/actor_3.jpg', 'assets/actor_4.jpg'];

    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorAccent,
        title: Text(
          'Movie Detail',
          style: textTitle.copyWith(fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            UniconsLine.arrow_circle_left,
            color: Colors.white,
          ),
        ),
      ),
      backgroundColor: colorAccent,
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            height: size.height * 0.5,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(dataFilm.path ?? 'asset/asset_burial.jpeg'),
            )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              dataFilm.title ?? 'Ini Judul',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w800),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 8,
                ),
                margin: const EdgeInsets.only(right: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(18),
                    border: Border.all(
                      color: colorGrey.withOpacity(0.5),
                    )),
                child: Text(
                  dataFilm.category ?? 'Kategori',
                  style: textTitle,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 24, 8, 0),
            child: Row(
              children: [
                const SizedBox(
                  height: 24,
                  child: Icon(
                    UniconsLine.clock,
                    size: 24,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  dataFilm.duration ?? '1j 0m',
                  style: textTitle,
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: Row(
              children: [
                const SizedBox(
                  height: 24,
                  child: Icon(
                    UniconsLine.user_circle,
                    size: 24,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  dataFilm.director ?? 'Ini direktor',
                  style: textTitle,
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: Row(
              children: [
                const SizedBox(
                  height: 24,
                  child: Icon(
                    UniconsLine.calender,
                    size: 24,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  dataFilm.year ?? '2012',
                  style: textTitle,
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Text(
              dataFilm.description ?? 'Lorem ipsum sir dolor amet',
              style: textTitle.copyWith(fontSize: 16, height: 1.5),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Text(
              'Cast',
              style: textTitle.copyWith(fontSize: 18, height: 1.5),
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            child: Container(
              margin: const EdgeInsets.only(left: 16, right: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: listActor.map((item) {
                  return Container(
                    margin: const EdgeInsets.only(right: 8),
                    child: SizedBox(
                      width: 150,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          ClipRRect(
                            borderRadius: const BorderRadius.all(Radius.circular(18)),
                            child: Container(
                              height: 200,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(item ?? ''),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          SizedBox(
            height: 100,
          ),
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(16),
        child: SizedBox(
          height: 48,
          width: double.infinity,
          child: TextButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute<void>(builder: (BuildContext context) {
                return const PaymentPage();
              }));
            },
            style: TextButton.styleFrom(
                backgroundColor: colorPrimary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                )),
            child: Text(
              'Berlangganan Sekarang',
              style: textTitle.copyWith(fontSize: 18),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
