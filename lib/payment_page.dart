import 'package:flutter/material.dart';
import 'package:hi_movie/colors.dart';
import 'package:hi_movie/payment_detail.dart';
import 'package:hi_movie/theme.dart';
import 'package:unicons/unicons.dart';

class PaymentPage extends StatefulWidget {
  const PaymentPage({Key? key}) : super(key: key);

  @override
  State<PaymentPage> createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  int cost = 0;
  int selectedSubscription = 0;
  int selectedPayment = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorAccent,
        title: Text(
          'Menu Berlangganan',
          style: textTitle.copyWith(fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            UniconsLine.arrow_circle_left,
            color: Colors.white,
            size: 24,
          ),
        ),
      ),
      backgroundColor: colorAccent.withOpacity(0.5),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: [
            Text(
              'Sistem Berlangganan',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w800),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        cost = 1000000;
                        selectedSubscription = 1;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: selectedSubscription == 1 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                        borderRadius: BorderRadius.circular(18.0),
                        border: Border.all(color: selectedSubscription == 1 ? colorPrimary : colorGrey),
                      ),
                      child: Column(
                        children: [
                          Text(
                            'Per Tahun',
                            style: textTitle,
                          ),
                          Text(
                            'Rp.1.000.000',
                            style: textTitle.copyWith(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        cost = 150000;
                        selectedSubscription = 2;
                      });
                    },
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: selectedSubscription == 2 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                        borderRadius: BorderRadius.circular(18.0),
                        border: Border.all(color: selectedSubscription == 2 ? colorPrimary : colorGrey),
                      ),
                      child: Column(
                        children: [
                          Text(
                            'Per Bulan',
                            style: textTitle,
                          ),
                          Text(
                            'Rp.150.000',
                            style: textTitle.copyWith(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              'Metode Pembayaran Bank',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w800),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selectedPayment = 1;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                decoration: BoxDecoration(
                  color: selectedPayment == 1 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: selectedPayment == 1 ? colorPrimary : Colors.grey),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      height: 24,
                      child: Image.asset('assets/ic_bca.png'),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Virtual Account BCA',
                      style: textTitle,
                    ),
                    Spacer(),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Icon(
                        selectedPayment == 1 ? UniconsLine.check_circle : UniconsLine.circle,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selectedPayment = 2;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                decoration: BoxDecoration(
                  color: selectedPayment == 2 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: selectedPayment == 2 ? colorPrimary : Colors.grey),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      height: 24,
                      child: Image.asset('assets/ic_bni.png'),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Virtual Account BNI',
                      style: textTitle,
                    ),
                    Spacer(),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Icon(
                        selectedPayment == 2 ? UniconsLine.check_circle : UniconsLine.circle,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selectedPayment = 3;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                decoration: BoxDecoration(
                  color: selectedPayment == 3 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: selectedPayment == 3 ? colorPrimary : Colors.grey),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      height: 24,
                      child: Image.asset('assets/ic_visa.png'),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Credit Card',
                      style: textTitle,
                    ),
                    Spacer(),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Icon(
                        selectedPayment == 3 ? UniconsLine.check_circle : UniconsLine.circle,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              'eWallet',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w800),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selectedPayment = 4;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                decoration: BoxDecoration(
                  color: selectedPayment == 4 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: selectedPayment == 4 ? colorPrimary : Colors.grey),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      height: 24,
                      child: Image.asset('assets/ic_gopay.png'),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Gopay',
                      style: textTitle,
                    ),
                    Spacer(),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Icon(
                        selectedPayment == 4 ? UniconsLine.check_circle : UniconsLine.circle,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  selectedPayment = 5;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                decoration: BoxDecoration(
                  color: selectedPayment == 5 ? colorPrimary.withOpacity(0.1) : Colors.transparent,
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: selectedPayment == 5 ? colorPrimary : Colors.grey),
                ),
                child: Row(
                  children: [
                    SizedBox(
                      height: 16,
                      child: Image.asset('assets/ic_ovo.jpeg'),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'OVO',
                      style: textTitle,
                    ),
                    const Spacer(),
                    SizedBox(
                      height: 24,
                      width: 24,
                      child: Icon(
                        selectedPayment == 5 ? UniconsLine.check_circle : UniconsLine.circle,
                        size: 24,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Total Bayar',
                      style: textTitle,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Rp.$cost',
                      style: textTitle,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TextButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(builder: (BuildContext context) {
                    return PaymentDetail(
                      amount: cost,
                    );
                  }));
                },
                style: TextButton.styleFrom(
                    backgroundColor: colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18),
                    )),
                child: Text(
                  'BAYAR',
                  style: textTitle.copyWith(fontSize: 14),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
