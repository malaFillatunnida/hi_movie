import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hi_movie/colors.dart';

ThemeData hiThemeData(BuildContext context) {
  return ThemeData(
      useMaterial3: true, primaryColor: colorPrimary, primaryColorDark: colorPrimaryDark, appBarTheme: AppBarTheme(backgroundColor: colorBackground), iconTheme: IconThemeData(color: colorHint));
}

TextStyle textHeadlineStyle = GoogleFonts.plusJakartaSans(color: Colors.white, fontSize: 24, fontWeight: FontWeight.w800);
TextStyle textTitle = GoogleFonts.plusJakartaSans(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600);
TextStyle textTitlePrimary = GoogleFonts.plusJakartaSans(color: colorPrimary, fontSize: 14, fontWeight: FontWeight.w600);
