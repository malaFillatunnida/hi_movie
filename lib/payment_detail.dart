import 'package:flutter/material.dart';
import 'package:hi_movie/colors.dart';
import 'package:hi_movie/success_page.dart';
import 'package:hi_movie/theme.dart';
import 'package:unicons/unicons.dart';

class PaymentDetail extends StatelessWidget {
  const PaymentDetail({Key? key, required this.amount}) : super(key: key);

  final int amount;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorAccent.withOpacity(0.5),
        title: Text(
          'Pembayaran Detail',
          style: textTitle.copyWith(fontSize: 18),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            UniconsLine.arrow_circle_left,
            color: Colors.white,
            size: 24,
          ),
        ),
      ),
      backgroundColor: colorAccent.withOpacity(0.5),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: [
            Text(
              'Batas akhir pembayaran',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            Text(
              'Minggu, 05 November 2023 10:48:00',
              style: textTitle.copyWith(fontSize: 16, fontWeight: FontWeight.w200),
            ),
            const SizedBox(height: 16),
            Divider(
              height: 20,
              thickness: 0.5,
              color: Colors.grey.withOpacity(0.5),
            ),
            const SizedBox(height: 16),
            Text(
              'Metode Pembayaran',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            Row(
              children: [
                Text(
                  'Virtual Account BCA',
                  style: textTitle.copyWith(fontSize: 16, fontWeight: FontWeight.w200),
                ),
                const Spacer(),
                SizedBox(
                  height: 24,
                  child: Image.asset('assets/ic_bca.png'),
                )
              ],
            ),
            const SizedBox(height: 16),
            Divider(
              height: 20,
              thickness: 0.5,
              color: Colors.grey.withOpacity(0.5),
            ),
            const SizedBox(height: 16),
            Text(
              'Nomor VA',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            Row(
              children: [
                Text(
                  '44408989999',
                  style: textTitle.copyWith(fontSize: 16, fontWeight: FontWeight.w200),
                ),
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Icon(
                    UniconsLine.copy_alt,
                    size: 24,
                    color: colorPrimary,
                  ),
                )
              ],
            ),
            const SizedBox(height: 16),
            Text(
              'Jumlah Pembayaran',
              style: textTitle.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            Text(
              'Rp. $amount',
              style: textTitle.copyWith(fontSize: 16, fontWeight: FontWeight.w200),
            ),
            const SizedBox(
              height: 24,
            ),
            Divider(
              height: 20,
              thickness: 0.5,
              color: Colors.grey.withOpacity(0.5),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              'Langganan kamu akan langsung aktif setelah pembayaran terverifikasi',
              style: textTitle,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 16,
            ),
            Divider(
              height: 20,
              thickness: 0.5,
              color: Colors.grey.withOpacity(0.5),
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                'LIHAT CARA BAYAR',
                style: textTitle.copyWith(fontSize: 16, color: colorPrimary),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 24,
                    width: 24,
                    child: Icon(
                      UniconsLine.shield_check,
                      size: 24,
                      color: Colors.green,
                    ),
                  ),
                  Text(
                    'Pembayaran ini dilindungi',
                    style: textTitle.copyWith(fontSize: 16, fontWeight: FontWeight.w200),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(16),
        child: SizedBox(
          height: 48,
          width: double.infinity,
          child: TextButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute<void>(builder: (BuildContext context) {
                return const SuccesPage();
              }));
            },
            style: TextButton.styleFrom(
                backgroundColor: colorPrimary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                )),
            child: Text(
              'CEK STATUS PEMBAYARAN',
              style: textTitle.copyWith(fontSize: 18),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
