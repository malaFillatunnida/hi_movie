import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hi_movie/route_app.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {


  @override
  void initState() {
    super.initState();
    Timer(const Duration(milliseconds: 2000), (){
      Navigator.of(context).pushNamedAndRemoveUntil(NavigatorRoutes.home, (Route<dynamic> route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: SizedBox(
            width: 200,
            height: 200,
            child: Image.asset('assets/ic_main.png'),
          ),
        ),
      ],
    );
  }
}
