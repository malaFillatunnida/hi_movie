class MovieModel {
  int? _id;
  String? _path;
  String? _title;
  String? _year;
  String? _duration;
  String? _category;
  String? _description;
  String? _director;

  MovieModel({
    int? id,
    String? path,
    String? title,
    String? year,
    String? duration,
    String? category,
    String? description,
    String? director,
  }) {
    _id = id;
    _path = path;
    _title = title;
    _year = year;
    _duration = duration;
    _category = category;
    _description = description;
    _director = director;
  }

  int? get id => _id;

  String? get path => _path;

  String? get title => _title;

  String? get year => _year;

  String? get duration => _duration;

  String? get category => _category;

  String? get description => _description;

  String? get director => _director;

  
}
